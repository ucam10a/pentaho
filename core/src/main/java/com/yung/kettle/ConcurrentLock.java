package com.yung.kettle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.pentaho.di.job.Job;

public class ConcurrentLock {

    /** current lock */
    private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock(true);
    
    /** lock entries */
    private static final ConcurrentHashMap<String, Job> lockJobs = new ConcurrentHashMap<String, Job>();
    private static final List<String> lockTrans = Collections.synchronizedList(new  ArrayList<String>());
    
    public static void lockTran(String entry){
        try {
            lock.writeLock().lock();
            lockTrans.add(entry);
        } finally {
            lock.writeLock().unlock();
        }
    }
    
    public static void removeTran(String entry){
        try {
            lock.writeLock().lock();
            lockTrans.remove(entry);
        } finally {
            lock.writeLock().unlock();
        }
    }
    
    public static void removeTrans(Set<String> entries){
        try {
            lock.writeLock().lock();
            lockTrans.removeAll(entries);
        } finally {
            lock.writeLock().unlock();
        }
    }
    
    
    public static void lockJob(String name, Job job){
        try {
            lock.writeLock().lock();
            if (lockJobs.get(name) != null) {
                throw new RuntimeException("job: " + name + " is running now");
            }
            lockJobs.put(name, job);
        } finally {
            lock.writeLock().unlock();
        }
    }
    
    public static void removeJob(String name){
        try {
            lock.writeLock().lock();
            lockJobs.remove(name);
        } finally {
            lock.writeLock().unlock();
        }
    }
    
    public static void lockTrans(Set<String> entries){
        try {
            lock.writeLock().lock();
            for (String entry : entries) {
                lockTrans.add(entry);
            }
        } finally {
            lock.writeLock().unlock();
        }
    }
    
    public static void clearAllTrans(){
        try {
            lock.writeLock().lock();
            lockTrans.clear();
        } finally {
            lock.writeLock().unlock();
        }
    }
    
    public static void clearAllJobs(){
        try {
            lock.writeLock().lock();
            lockJobs.clear();
        } finally {
            lock.writeLock().unlock();
        }
    }
    
    public static List<String> getAllLockEntries(){
        try {
            lock.readLock().lock();
            List<String> copy = new ArrayList<String>();
            copy.addAll(lockTrans);
            copy.addAll(lockJobs.keySet());
            return copy;
        } finally {
            lock.readLock().unlock();
        }
    }
    
    public static List<String> getLockTrans(){
        try {
            lock.readLock().lock();
            List<String> copy = new ArrayList<String>();
            copy.addAll(lockTrans);
            return copy;
        } finally {
            lock.readLock().unlock();
        }
    }
    
    public static List<String> getLockJobs(){
        try {
            lock.readLock().lock();
            List<String> copy = new ArrayList<String>();
            copy.addAll(lockJobs.keySet());
            return copy;
        } finally {
            lock.readLock().unlock();
        }
    }
    
    public static Job getLockJob(String jobKey){
        try {
            lock.readLock().lock();
            return lockJobs.get(jobKey);
        } finally {
            lock.readLock().unlock();
        }
    }

    public static void clearWriteLock() {
        if (lock.isWriteLocked()){
            if (lock.writeLock().isHeldByCurrentThread()) {
                lock.writeLock().unlock();
            }
        }
    }
    
}
