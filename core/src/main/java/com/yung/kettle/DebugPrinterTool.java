package com.yung.kettle;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * For debug only
 * 
 * @author Yung Long Li
 * 
 */
public class DebugPrinterTool {

    private static final Log log = LogFactory.getLog(DebugPrinterTool.class);
    
    public static void printMarkMessage(String message) {
        log.info("----------------------------------------------------------------" + message);
    }

    private static void printMarkMessage(String indent, String message) {
        if (indent == null || indent.equals("")) {
            printMarkMessage(message);
        } else {
            log.info(indent + message);
        }
    }

    public static void printParam(String paramName, String value) {
        log.info(paramName + " = " + value);
    }

    public static void printList(String indent, String objName, List<?> list) {
        try {
            int i = 1;
            printMarkMessage(indent, list.getClass().getName() + ": " + objName);
            for (Object obj : list) {
                printObjectParam(indent + "    ", objName + i + ": ", obj, false);
                i++;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void printSet(String indent, String objName, Set<?> set) {
        try {
            int i = 1;
            printMarkMessage(indent, set.getClass().getName() + ": " + objName);
            for (Object obj : set) {
                printObjectParam(indent + "    ", objName + i + ": ", obj, false);
                i++;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void printMap(String indent, String objName, Map<?, ?> map) {
        try {
            int i = 1;
            printMarkMessage(indent, map.getClass().getName() + ": " + objName);
            for (Entry<?, ?> entry : map.entrySet()) {
                printObjectParam(indent + "    ", "key" + i + ":", entry.getKey(), false);
                printObjectParam(indent + "    ", "value" + i, entry.getValue(), false);
                i++;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void printArray(String indent, String objName, Object[] array) {
        try {
            int i = 1;
            printMarkMessage(indent, array.getClass().getName() + ": " + objName);
            for (Object obj : array) {
                printObjectParam(indent + "    ", objName + i + ": ", obj, false);
                i++;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void printObjectParam(String indent, String objName, Object obj) {
        printObjectParam(indent, objName, obj, true);
    }

    public static void printObjectParam(String indent, String objName, Object obj, boolean printTitle) {
        try {
            if (indent.length() > 200) {
                return;
            }
            if (obj == null) {
                printMarkMessage(indent, "Object:" + objName + " is null");
                return;
            }
            if (obj instanceof List) {
                List<?> list = (List<?>) obj;
                printList(indent, objName, list);
            } else if (obj.getClass().isArray()) {
                Object[] array = (Object[]) obj;
                printArray(indent, objName, array);
            } else if (obj instanceof Set) {
                Set<?> set = (Set<?>) obj;
                printSet(indent, objName, set);
            } else if (obj instanceof Map) {
                Map<?, ?> map = (Map<?, ?>) obj;
                printMap(indent, objName, map);
            } else {
                if (allowType(obj.getClass())) {
                    printMarkMessage(indent, objName + ": " + obj.toString());
                } else {
                    Class<?> cls = obj.getClass();
                    if (printTitle) {
                        printMarkMessage(indent, "object name:" + objName + ", " + cls.getName());
                    } else {
                        printMarkMessage(indent, objName + ", " + cls.getName());
                    }
                    // print field
                    while (cls != null) {
                        Field[] fields = cls.getDeclaredFields();
                        indent = "    " + indent;
                        for (Field f : fields) {
                            Object returnObj = runGetter(f, obj);
                            if (returnObj == null) {
                                log.info(indent + f.getName() + " is null!");
                                continue;
                            }
                            String value = null;
                            if (returnObj instanceof List) {
                                List<?> list = (List<?>) returnObj;
                                printList(indent, f.getName(), list);
                            } else if (returnObj.getClass().isArray()) {
                                Object[] array = (Object[]) returnObj;
                                printArray(indent, f.getName(), array);
                            } else if (returnObj instanceof Set) {
                                Set<?> set = (Set<?>) returnObj;
                                printSet(indent, f.getName(), set);
                            } else if (returnObj instanceof Map) {
                                Map<?, ?> map = (Map<?, ?>) returnObj;
                                printMap(indent, f.getName(), map);
                            } else {
                                if (allowType(f.getType())) {
                                    value = returnObj.toString();
                                    printParam(indent + f.getName(), value);
                                } else {
                                    printObjectParam(indent, f.getName(), returnObj);
                                }
                            }
                        }
                        cls = cls.getSuperclass();
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static boolean allowType(Class<?> cls) {
        if (cls == String.class) {
            return true;
        } else if (cls == Integer.class) {
            return true;
        } else if (cls == Double.class) {
            return true;
        } else if (cls == Float.class) {
            return true;
        } else if (cls == Long.class) {
            return true;
        } else if (cls == Short.class) {
            return true;
        } else if (cls == BigInteger.class) {
            return true;
        } else if (cls == BigDecimal.class) {
            return true;
        } else if (cls == Boolean.class) {
            return true;
        } else if (cls == Date.class) {
            return true;
        }
        return false;
    }

    private static Object runGetter(Field field, Object obj) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        if (field.getType() == Boolean.class) {
            return runBooleanGetter(field.getName(), obj);
        } else {
            return runGetter(field.getName(), obj);
        }
    }

    private static Object runGetter(String fieldName, Object obj) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        for (Method method : obj.getClass().getMethods()) {
            if ((method.getName().startsWith("get")) && (method.getName().length() == (fieldName.length() + 3))) {
                if (method.getName().toLowerCase().endsWith(fieldName.toLowerCase())) {
                    if (method.getParameterTypes().length == 0) {
                        return method.invoke(obj);
                    }
                }
            }
        }
        return null;
    }

    private static Object runBooleanGetter(String fieldName, Object obj) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        for (Method method : obj.getClass().getMethods()) {
            if ((method.getName().startsWith("is")) && (method.getName().length() == (fieldName.length() + 2))) {
                if (method.getName().toLowerCase().endsWith(fieldName.toLowerCase())) {
                    return method.invoke(obj);
                }
            }
        }
        return null;
    }

    public static void printlnClasssMessage(Object obj, String string) {
        log.info(obj.getClass().getName() + ":  " + string);
    }

}