package com.yung.kettle;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.Result;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.job.Job;
import org.pentaho.di.job.JobMeta;
import org.pentaho.di.repository.Repository;
import org.pentaho.di.repository.RepositoryDirectoryInterface;
import org.pentaho.di.repository.kdr.KettleDatabaseRepository;
import org.pentaho.di.repository.kdr.KettleDatabaseRepositoryMeta;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;

public class KettleTask {
    
    /** logger */
    private static final Log log = LogFactory.getLog(KettleTask.class);
    
    public static Result executeTransFromDatabase(String name, String type, String access
            , String host, String db, String port, String dbUser, String dbPass
            , String repoUser, String repoPass
            , String repoDirectory, String tranName) throws KettleException {
        try {
            // initialize kettle environment
            KettleEnvironment.init();
            // create database repository
            KettleDatabaseRepository repository = new KettleDatabaseRepository();
            // create database meta 
            DatabaseMeta dataMeta = new DatabaseMeta(name, type, access, host, db, port, dbUser, dbPass);
            System.out.println(dataMeta.getURL());
            // name, id, description defined by user
            KettleDatabaseRepositoryMeta kettleDatabaseMeta = new KettleDatabaseRepositoryMeta(type, "test", "test", dataMeta);
            // put database meta into repository
            repository.init(kettleDatabaseMeta);
            // connect database
            repository.connect(repoUser, repoPass);
            // lookup directory 
            RepositoryDirectoryInterface directory = repository.findDirectory(repoDirectory);
            // get transmeta
            TransMeta transformationMeta = ((Repository) repository).loadTransformation(tranName, directory, null, true, null);
            // create Trans
            Trans trans = new Trans(transformationMeta);
            log.info("starting transformation: " + name);
            // execute
            trans.execute(null);
            // wait for finished
            trans.waitUntilFinished();
            if (trans.getErrors() > 0) {
                log.error("Transformation: " + tranName + " has error!");
            } else {
                log.info("Transformation: " + tranName + " run successfully!");
            }
            return trans.getResult();
        } catch (KettleException e) {
            log.error(e.toString(), e);
            throw e;
        }
    }
    
    public static Result executeJobFromDatabase(String name, String type, String access
            , String host, String db, String port, String dbUser, String dbPass
            , String repoUser, String repoPass
            , String repoDirectory, String jobName) throws KettleException {
        try {
            // initialize kettle environment
            KettleEnvironment.init();
            // create database repository
            KettleDatabaseRepository repository = new KettleDatabaseRepository();
            // create database meta 
            DatabaseMeta dataMeta = new DatabaseMeta(name, type, access, host, db, port, dbUser, dbPass);
            System.out.println(dataMeta.getURL());
            // name, id, description defined by user
            KettleDatabaseRepositoryMeta kettleDatabaseMeta = new KettleDatabaseRepositoryMeta(type, "test", "test", dataMeta);
            // put database meta into repository
            repository.init(kettleDatabaseMeta);
            // connect database
            repository.connect(repoUser, repoPass);
            // lookup directory 
            RepositoryDirectoryInterface directory = repository.findDirectory(repoDirectory);
            // get job meta
            JobMeta jobMeta = ((Repository) repository).loadJob(jobName, directory, null, null);
            // create job
            Job job = new Job(repository, jobMeta, jobMeta);
            log.info("starting job: " + name);
            // execute
            job.start();
            // wait for finished
            job.waitUntilFinished();
            if (job.getErrors() > 0) {
                log.warn("Job: " + jobName + " has error!");
            } else {
                log.info("Job: " + jobName + " run successfully!");
            }
            return job.getResult();
        } catch (KettleException e) {
            log.error(e.toString(), e);
            throw e;
        }
    }

    public static Result executeTransFromFile(String fname) throws KettleException {
        try {
            KettleEnvironment.init();
            TransMeta metaData = new TransMeta(fname);
            Trans trans = new Trans(metaData);
            log.info("starting transformation: " + fname);
            trans.execute(null);
            trans.waitUntilFinished();
            if ( trans.getErrors() > 0 ) {
                log.error("Executing transformation: " + fname + " has error!");
            } else {
                log.info("Executing transformation " + fname + " done");
            }
            return trans.getResult();
        } catch( KettleException e ) {
            log.error(e.toString(), e);
            throw e;
        }
    }

    public static Result executeJob(String fname) throws KettleException {
        Result result = null;
        boolean holdLock = false;
        JobMeta jobMeta = new JobMeta(fname, null);
        Job job = new Job(null, jobMeta);
        try {
            ConcurrentLock.lockJob(fname, job);
            holdLock = true;
            result = executeJobFromFile(fname);
            return result;
        } finally {
            if (holdLock) {
                ConcurrentLock.removeJob(fname);
            }
        }
    }
    
    private static Result executeJobFromFile(String fname) throws KettleException {
        try {
            KettleEnvironment.init();
            JobMeta jobMeta = new JobMeta(fname, null);
            Job job = new Job(null, jobMeta);
            log.info("starting job: " + fname);
            ConcurrentLock.lockJob(fname, job);
            job.start();
            job.waitUntilFinished();
            if (job.getErrors() != 0) {
                log.error("Executing job: " + fname + " has error!");
            } else {
                log.info("Executing job " + fname + " done");
            }
            Result result = job.getResult();
            String lastEntryName = job.getJobEntryResults().get(job.getJobEntryResults().size() - 1).getJobEntryName();
            if (lastEntryName.equalsIgnoreCase("success")) {
                log.info("job success");
                log.info("job log text \n" + result.getLogText());
            } else {
                log.info("job stopped");
            }
            return result;
        } catch (KettleException e) {
            log.error(e.toString(), e);
            throw e;
        }
    }
    
    public static void stopJob(String fname) throws KettleException {
        Job job = ConcurrentLock.getLockJob(fname);
        if (job != null) {
            log.info("Stopping job " + fname);
            job.stopAll();
            log.info("Job: " + fname + " stopped");
        } else {
            log.warn("can not find job: " + fname);
        } 
    }
    
    public static void main(String[] args) throws Exception {
        
        // test lock
        ExecutorService executorService2 = Executors.newFixedThreadPool(10);
        final Random ran = new Random();
        
        executorService2.execute(new Runnable() {
            public void run() {
                try {
                    try {
                        Thread.sleep(ran.nextInt(10000));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("start job1");
                    executeJob("job");
                    System.out.println("lock job1 done");
                } catch (Exception e) {
                    System.out.println("run job1 fail");
                }
            }
        });
        
        executorService2.execute(new Runnable() {
            public void run() {
                try {
                    try {
                        Thread.sleep(ran.nextInt(10000));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("start job2");
                    executeJob("job");
                    System.out.println("lock job2 done");
                } catch (Exception e) {
                    System.out.println("run job2 fail");
                }
            }
        });
        
        executorService2.execute(new Runnable() {
            public void run() {
                try {
                    try {
                        Thread.sleep(ran.nextInt(10000));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("start job3");
                    executeJob("job");
                    System.out.println("lock job3 done");
                } catch (Exception e) {
                    System.out.println("run job3 fail");
                }
            }
        });
        
        executorService2.execute(new Runnable() {
            public void run() {
                try {
                    try {
                        Thread.sleep(ran.nextInt(10000));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("start job4");
                    executeJob("job");
                    System.out.println("lock job4 done");
                } catch (Exception e) {
                    System.out.println("run job4 fail");
                }
            }
        });
        
        executorService2.execute(new Runnable() {
            public void run() {
                try {
                    try {
                        Thread.sleep(ran.nextInt(10000));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("start job5");
                    executeJob("job");
                    System.out.println("lock job5 done");
                } catch (Exception e) {
                    System.out.println("run job5 fail");
                }
            }
        });
        
        
        executorService2.shutdown();
        System.out.println("done");
        
    }
    
}
