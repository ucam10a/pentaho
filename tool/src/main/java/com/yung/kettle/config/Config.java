package com.yung.kettle.config;

import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Config {

    /** logger */
    private static final Log log = LogFactory.getLog(Config.class);
    
    private static String dbType;
    
    private static String dbAccess;
    
    private static String dbHost;
    
    private static String dbSchema;
    
    private static String dbPort;
    
    private static String dbUser;
    
    private static String dbPass;
    
    private static String repoUser;
    
    private static String repoPass;
    
    private static String repoPath;

    static {
        ClassLoader loader = Config.class.getClassLoader();
        try {
            Properties prop = new Properties();
            prop.load(loader.getResourceAsStream("config.properties"));
            // get property and set value
            dbType = prop.getProperty("db.type");
            dbAccess = prop.getProperty("db.access");
            dbHost = prop.getProperty("db.host");
            dbSchema = prop.getProperty("db.schema");
            dbPort = prop.getProperty("db.port");
            dbUser = prop.getProperty("db.user");
            dbPass = prop.getProperty("db.pass");
            repoUser = prop.getProperty("repo.user");
            repoPass = prop.getProperty("repo.pass");
            repoPath = prop.getProperty("repo.path");
        } catch (Exception e) {
            log.error(e.toString(), e);
        }
    }
    
    public static String getDbType() {
        return dbType;
    }

    public static String getDbAccess() {
        return dbAccess;
    }

    public static String getDbHost() {
        return dbHost;
    }

    public static String getDbSchema() {
        return dbSchema;
    }

    public static String getDbPort() {
        return dbPort;
    }

    public static String getDbUser() {
        return dbUser;
    }

    public static String getDbPass() {
        return dbPass;
    }

    public static String getRepoUser() {
        return repoUser;
    }

    public static String getRepoPass() {
        return repoPass;
    }

    public static String getRepoPath() {
        return repoPath;
    }

}
