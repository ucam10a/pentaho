package com.yung.kettle.orm;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

import com.yung.util.FileUtil;

public abstract class AbstractKettleTask {

    protected static final String JOB_TYPE = "job";
    
    protected static final String TRANSFORMATION_TYPE = "trans";
    
    private String name;
    
    private String cronExpression;
    
    private String type;
    
    private boolean running = false;

    public String getName() {
        return name;
    }

    public void setName(String name) throws UnsupportedEncodingException, URISyntaxException {
        name = FileUtil.getFileURI(name);
        this.name = name;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    protected String getType() {
        return type;
    }

    protected void setType(String type) {
        this.type = type;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }
    
}
