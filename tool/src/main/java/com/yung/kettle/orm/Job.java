package com.yung.kettle.orm;

import java.util.LinkedHashSet;

public class Job extends AbstractKettleTask {

    private LinkedHashSet<String> subJobs = new LinkedHashSet<String>();
    
    private LinkedHashSet<String> trans = new LinkedHashSet<String>();
    
    public Job() {
        setType(AbstractKettleTask.JOB_TYPE);
    }

    public LinkedHashSet<String> getTrans() {
        return trans;
    }

    public void setTrans(LinkedHashSet<String> trans) {
        this.trans = trans;
    }

    public LinkedHashSet<String> getSubJobs() {
        return subJobs;
    }

    public void setSubJobs(LinkedHashSet<String> subJobs) {
        this.subJobs = subJobs;
    }
    
}
