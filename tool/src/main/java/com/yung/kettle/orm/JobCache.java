package com.yung.kettle.orm;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.pentaho.di.core.exception.KettleXMLException;
import org.pentaho.di.job.JobMeta;
import org.pentaho.di.job.entries.job.JobEntryJob;
import org.pentaho.di.job.entries.trans.JobEntryTrans;
import org.pentaho.di.job.entry.JobEntryCopy;
import org.pentaho.di.job.entry.JobEntryInterface;

import com.yung.kettle.config.Config;
import com.yung.util.FileUtil;

public class JobCache {

    private static Map<String, Job> cache = new ConcurrentHashMap<String, Job>();
    
    public static Job getJob(File jobFile) throws KettleXMLException, UnsupportedEncodingException, URISyntaxException{
        Job result = cache.get(jobFile.getAbsolutePath());
        if (result == null) {
            result = new Job();
            JobMeta jobMeta = new JobMeta(jobFile.getAbsolutePath(), null);
            LinkedHashSet<String> subJobs = new LinkedHashSet<String>();
            LinkedHashSet<String> jobTrans = new LinkedHashSet<String>();
            List<JobEntryCopy> entries = jobMeta.getJobCopies();
            for (JobEntryCopy copy : entries) {
                JobEntryInterface entry = copy.getEntry();
                if (entry instanceof JobEntryTrans) {
                    String childTran = FileUtil.getFile(entry.getFilename()).getName();
                    jobTrans.add(childTran);
                } else if (entry instanceof JobEntryJob) {
                    Job subJob = getJob(FileUtil.getFile(jobFile.getParentFile().getAbsolutePath() + "/" + parseFilename(entry.getFilename())));
                    subJobs.add(subJob.getName());
                    jobTrans.addAll(subJob.getTrans());
                }
            }
            result.setSubJobs(subJobs);
            result.setTrans(jobTrans);
            result.setName(jobFile.getAbsolutePath());
            cache.put(jobFile.getAbsolutePath(), result);
        }
        return result;
    }
    
    public static String getRepoBase() {
        String repoBase = Config.getRepoPath();
        if (repoBase == null || repoBase.equals("")) {
            repoBase = JobCache.class.getClassLoader().getResource("").toString();
        }
        return repoBase;
    }
    
    public static String parseFilename(String path) {
        String name = path;
        int idx = name.lastIndexOf("/");
        if (idx != -1) {
            name = name.substring(idx + 1);
        }
        return name;
    }
}
