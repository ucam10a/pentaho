package com.yung.kettle.orm;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import com.yung.util.FileUtil;

public class Transformation extends AbstractKettleTask {

    private List<String> parentJobs = new ArrayList<String>();
    
    public Transformation() {
        setType(AbstractKettleTask.TRANSFORMATION_TYPE);
    }

    public List<String> getParentJobs() {
        return parentJobs;
    }

    public void setParentJobs(List<String> parentJobs) {
        this.parentJobs = parentJobs;
    }
    
    public void seekParentJobs(List<Job> jobs) throws UnsupportedEncodingException, URISyntaxException {
        for (Job job : jobs) {
            if (job.getTrans().contains(FileUtil.getFile(getName()).getName())) {
                parentJobs.add(job.getName());
            }
        }
    }
    
}
