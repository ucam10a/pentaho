package com.yung.kettle.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.pentaho.di.core.exception.KettleException;

import com.yung.kettle.KettleTask;

public class CancelServlet extends HttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /** logger */
    private static final Log log = LogFactory.getLog(CancelServlet.class);

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try {
            String fname = request.getParameter(ExecuteServlet.FNAME);
            KettleTask.stopJob(fname);
            List<String> logs = new ArrayList<String>();
            logs.add(fname + "  cancel successfully!");
            logs.add("Note: the transformation process which is running will not terminated until it is done.");
            logs.add("However all other un-executed transformation will not fired!");
            request.setAttribute("logs", logs);
            request.getRequestDispatcher("success.jsp").forward(request, response);
        } catch (KettleException e) {
            log.error(e.toString(), e);
        }
    }
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doGet(request, response);
    }
    
}
