package com.yung.kettle.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.pentaho.di.core.Result;

import com.yung.kettle.ConcurrentLock;
import com.yung.kettle.KettleTask;
import com.yung.kettle.orm.JobCache;
import com.yung.util.FileUtil;

/**
 * An Servlet example to remote execute kettle Transformation job
 * 
 * @author Yung Long Li
 *
 */
public class ExecuteServlet extends HttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public static final String FNAME = "fname";
    public static final String TYPE = "type";
    public static final String DIRECTORY = "directory";
    
    public static final String TYPE_JOB = "job";
    public static final String TYPE_TRAN = "tran";
    
    /** logger */
    private static final Log log = LogFactory.getLog(ExecuteServlet.class);
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try {
            log.debug("ExecuteServlet start ...");
            String fname = request.getParameter(FNAME);
            String type = request.getParameter(TYPE);
            log.debug("filePath: " + fname);
            //String directory = (String) request.getAttribute(DIRECTORY);
            //log.debug("directory: " + directory);
            //boolean result = KettleTask.executeJobFromDatabase(Config.getDbType() + "Repo", Config.getDbType(), Config.getDbAccess()
            //        , Config.getDbHost(), Config.getDbSchema(), Config.getDbPort(), Config.getDbUser(), Config.getDbPass()
            //        , Config.getRepoUser(), Config.getRepoPass()
            //        , directory, fname);
            try {
                Result result;
                List<String> runningEntries = ConcurrentLock.getAllLockEntries();
                log.debug("runningEntries: " + runningEntries);
                if (type.equals(TYPE_JOB)){
                    LinkedHashSet<String> exeTrans = JobCache.getJob(FileUtil.getFile(fname)).getTrans();
                    for (String tran : exeTrans) {
                        if (runningEntries.contains(tran)) {
                            throw new Exception(tran + " is running!");
                        }
                    }
                    ConcurrentLock.lockTrans(exeTrans);
                    result = KettleTask.executeJob(fname);
                    ConcurrentLock.removeTrans(exeTrans);
                } else if (type.equals(TYPE_TRAN)){
                    if (runningEntries.contains(fname)) {
                        throw new Exception(fname + " is running!");
                    }
                    ConcurrentLock.lockTran(fname);
                    result = KettleTask.executeTransFromFile(fname);
                    ConcurrentLock.removeTran(fname);
                } else {
                    throw new Exception("Unknown type: " + type);
                }
                request.setAttribute("logs", getLogs(result.getLogText()));
                if (result.getNrErrors() == 0) {
                    request.getRequestDispatcher("success.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("error.jsp").forward(request, response);
                }
                return;
            } finally {
                // to prevent dead lock
                ConcurrentLock.clearWriteLock();
            }
        } catch (Exception e) {
            log.error(e.toString(), e);
            request.setAttribute("errorMsg", e.toString());
            request.getRequestDispatcher("fail.jsp").forward(request, response);
            return;
        }
    }
    
    private List<String> getLogs(String log) {
        List<String> logResult = new ArrayList<String>();
        Scanner scanner = new Scanner(log);
        while (scanner.hasNextLine()) {
          String line = scanner.nextLine();
          if (line != null) {
              line = line.trim();
              if (!line.equals("") && !line.equals("null")) {
                  logResult.add(line);
              }
          }
        }
        scanner.close();
        return logResult;
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doGet(request, response);
    }
    
}
