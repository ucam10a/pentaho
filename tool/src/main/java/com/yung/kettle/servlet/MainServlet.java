package com.yung.kettle.servlet;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.pentaho.di.core.KettleEnvironment;

import com.yung.kettle.ConcurrentLock;
import com.yung.kettle.config.Config;
import com.yung.kettle.orm.Job;
import com.yung.kettle.orm.JobCache;
import com.yung.kettle.orm.Transformation;
import com.yung.util.FileUtil;

public class MainServlet extends HttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /** logger */
    private static final Log log = LogFactory.getLog(MainServlet.class);
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try {
            List<Job> jobs = new ArrayList<Job>();
            List<Transformation> trans = new ArrayList<Transformation>();
            String repoBase = request.getParameter("repoBase");
            List<File> files = getKettleFileList(repoBase);
            KettleEnvironment.init();
            List<String> runningEntries = ConcurrentLock.getLockTrans();
            for (File file : files) {
                if (file.getAbsolutePath().endsWith("ktr")) {
                    Transformation tran = new Transformation();
                    tran.setName(file.getAbsolutePath());
                    if (runningEntries.contains(file.getName())) {
                        tran.setRunning(true);
                    }
                    trans.add(tran);
                } else if (file.getAbsolutePath().endsWith("kjb")) {
                    Job job = JobCache.getJob(file);
                    for (String tran : job.getTrans()) {
                        if (runningEntries.contains(tran)) {
                            job.setRunning(true);
                            break;
                        }
                    }
                    jobs.add(job);
                }
            }
            request.setAttribute("jobs", jobs);
            for (Transformation tran : trans) {
                tran.seekParentJobs(jobs);
            }
            request.setAttribute("trans", trans);
            request.getRequestDispatcher("main.jsp").forward(request, response);
        } catch (Exception e) {
            log.error(e.toString(), e);
            request.setAttribute("errorMsg", e.toString());
            request.getRequestDispatcher("fail.jsp").forward(request, response);
            return;
        }
    }
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doGet(request, response);
    }
    
    private List<File> getKettleFileList(String repoBase) throws UnsupportedEncodingException, URISyntaxException {
        List<File> results = new ArrayList<File>();
        if (repoBase == null) repoBase = Config.getRepoPath();
        if (repoBase == null || repoBase.equals("")) {
            repoBase = ExecuteServlet.class.getClassLoader().getResource("").toString();
        }
        File resourceDir = FileUtil.getFile(repoBase);
        List<String> fileList = new ArrayList<String>();
        List<String> pattern = new ArrayList<String>();
        pattern.add(".ktr");
        pattern.add(".kjb");
        FileUtil.generateFileList(resourceDir, fileList, pattern);
        for (String f : fileList) {
            results.add(FileUtil.getFile(resourceDir.getAbsolutePath() + "/" + f));
        }
        return results;
    }
    
}
