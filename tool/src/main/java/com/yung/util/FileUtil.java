package com.yung.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * File Utility
 * 
 * @author Yung Long Li
 *
 */
public class FileUtil {

    /**
     * recursive to get the all sub folder file
     * 
     * @param node
     *            current folder
     * @param fileList
     *            collecting file list container
     * @throws URISyntaxException
     * @throws UnsupportedEncodingException
     */
    public static void generateFileList(File node, List<String> fileList) throws URISyntaxException, UnsupportedEncodingException {
        String nodeURI = node.toURI().toString();
        generateFileList(nodeURI, node, fileList, null);
    }

    /**
     * recursive to get the all sub folder file
     * 
     * @param node
     *            node current folder
     * @param fileList
     *            fileList collecting file list container
     * @param patterns
     *            file name pattern
     * @throws URISyntaxException
     * @throws UnsupportedEncodingException
     */
    public static void generateFileList(File node, List<String> fileList, List<String> patterns) throws URISyntaxException, UnsupportedEncodingException {
        String nodeURI = node.toURI().toASCIIString();
        generateFileList(nodeURI, node, fileList, null);
    }

    /**
     * recursive to get the all sub folder file
     * 
     * @param source_folder
     *            source folder path
     * @param node
     *            current folder
     * @param fileList
     *            collecting file list container
     * @param patterns
     *            file name pattern
     * @throws URISyntaxException
     * @throws UnsupportedEncodingException
     */
    private static void generateFileList(String source_folder, File node, List<String> fileList, List<String> patterns) throws URISyntaxException, UnsupportedEncodingException {
        String source_folder_uri = getFileURI(source_folder);
        // add file only
        if (node.isFile()) {
            if (patterns != null) {
                String name = node.toURI().toString();
                for (String pattern : patterns) {
                    if (name.contains(pattern)) {
                        fileList.add(generateHierarchy(source_folder_uri, node.toURI().toString()));
                        break;
                    }
                }
            } else {
                fileList.add(generateHierarchy(source_folder_uri, node.toURI().toString()));
            }
        }
        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileList(source_folder, new File(node, filename), fileList, patterns);
            }
        }
    }

    /**
     * get File URI
     * 
     * @param source_folder
     *            source folder
     * @return URI string
     * @throws URISyntaxException
     * @throws UnsupportedEncodingException
     */
    public static String getFileURI(String source_folder) throws URISyntaxException, UnsupportedEncodingException {
        File temp = getFile(source_folder);
        return temp.toURI().toString();
    }

    /**
     * create file folder
     * 
     * @param filePath
     *            file path
     */
    public static void generateDir(String filePath) {
        String path = "";
        if (filePath.startsWith("file:")) {
            path = filePath.substring(6, filePath.length());
        } else {
            path = filePath;
        }
        String[] dirs = path.split("/");
        String createDir = "";
        for (int i = 0; i < dirs.length - 1; i++) {
            if (createDir.equals("")) {
                createDir = dirs[i];
            } else {
                createDir = createDir + "/" + dirs[i];
            }
            File fDir = new File(createDir);
            if (!fDir.exists()) {
                fDir.mkdirs();
            }
        }
    }

    /**
     * get sub folder path to zip file with a tree hierarchy
     * 
     * @param source_folder
     *            parent folder path
     * @param file
     *            sub folder name
     * @return sub folder path
     */
    private static String generateHierarchy(String source_folder, String file) {
        int index = file.indexOf(source_folder);
        if (index == -1) {
            throw new RuntimeException("file path error !");
        } else {
            return file.substring(index + source_folder.length(), file.length());
        }
    }

    /**
     * get file
     * 
     * @param filePath
     *            file path
     * @return file
     * @throws URISyntaxException
     * @throws UnsupportedEncodingException
     */
    public static File getFile(String filePath) throws URISyntaxException, UnsupportedEncodingException {
        File file = null;
        if (filePath.startsWith("file:")) {
            file = new File(new URI(filePath));
        } else {
            filePath = URLDecoder.decode(filePath, "UTF-8");
            file = new File(filePath);
        }
        return file;
    }

    /**
     * copy file
     * 
     * @param srcFile
     *            source file
     * @param desFile
     *            target file path
     * @throws IOException
     * @throws URISyntaxException
     */
    public static void fileCopy(File srcFile, String desFile) throws IOException, URISyntaxException {
        InputStream in = new FileInputStream(srcFile);
        File file = getFile(desFile);
        OutputStream out = new FileOutputStream(file);
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    /**
     * copy folder
     * 
     * @param srcPath
     *            source folder path
     * @param destPath
     *            destination folder path
     * @throws Exception
     */
    public static void copyToNewFolder(String srcPath, String destPath) throws Exception {
        File srcFile = getFile(srcPath);
        if (!srcFile.isDirectory()) {
            throw new Exception(srcPath + " is not a directory!");
        }
        File destFile = getFile(destPath);
        if (destFile.exists()) {
            if (destFile.listFiles().length != 0) {
                throw new Exception(destPath + " is not empty!");
            }
        }
        List<String> files = new ArrayList<String>();
        generateFileList(srcFile, files, null);
        for (String f : files) {
            System.out.println(f);
            generateDir(destPath + "/" + f);
            fileCopy(getFile(srcPath + "/" + f), destPath + "/" + f);
        }
    }

    /**
     * get file name
     * 
     * @param filename
     *            full file name path
     * @return file name
     */
    public static String getFilename(String filename) {
        int index = filename.lastIndexOf("/");
        if (index == filename.length()) return null;
        return filename.substring(index + 1, filename.length());
    }

    /**
     * test method or ant command
     * 
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        String srcDir = "file:/D:/Users/frank/code/TKSS/copy/src";
        String toDir = "D:/Users/frank/code/TKSS/test-copy";

        List<String> patterns = new ArrayList<String>();
        patterns.add("wiki");

        copyToNewFolder(srcDir, toDir);

        System.out.println("done");

    }

}