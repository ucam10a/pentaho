<%@ page language="java" contentType="text/html; charset=BIG5" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=BIG5">
    <title>Main Page</title>
    <script>
        function cancelKettleJob(jobName) {
            document.getElementById('cancel-job-fname').value = jobName;
            document.cancelKettleJobForm.submit();
        }
        function exeKettleJob(jobName) {
        	document.getElementById('job-fname').value = jobName;
            document.exeKettleJobForm.submit();
        }
        function exeKettleTran(tranName) {
        	document.getElementById('tran-fname').value = tranName;
            document.exeKettleTranForm.submit();
        }
    </script>
</head>
<body>

    <h2> Kettle Job List </h2>
    <table border="1">
        <tr height="30px">
            <td><h3>Job Name</h3></td>
            <td><h3>Child Transformation</h3></td>
            <td><h3>Cron Expression</h3></td>
            <td><h3>Next Run Time</h3></td>
            <td><h3>is Running</h3></td>
            <td><h3>Execute</h3></td>
        </tr>
        <c:forEach items="${ jobs }" var="job">
            <tr height="30px">
                <td>
                    <c:out value="${job.name}" />
                    <c:forEach items="${ job.subJobs }" var="subJob">
                        <br> &nbsp; &nbsp; -- <c:out value="${subJob}" />
                    </c:forEach>
                </td>
                <td>
                    <c:forEach items="${ job.trans }" var="tran">
                        <c:out value="${tran}" /><br>
                    </c:forEach>
                </td>
                <td>Cron Expression</td>
                <td>Next Run Time</td>
                <td><c:out value="${ job.running }" /></td>
                <td> &nbsp; <input type="button" value="Execute" onclick="exeKettleJob('<c:out value="${job.name}" />');" /> <br><br>
                     &nbsp; <input type="button" value="Cancel" onclick="cancelKettleJob('<c:out value="${job.name}" />');" />
                </td>
            </tr>
        </c:forEach>
    </table>
    <div style="display: none;">
        <form id="exeKettleJobForm" name="exeKettleJobForm" action="/kettle/executeServlet" method="POST">
            <input id="job-fname" type="text" name="fname" value="" />
            <input type="text" name="type" value="job" />
        </form>
        <form id="cancelKettleJobForm" name="cancelKettleJobForm" action="/kettle/cancel" method="POST">
            <input id="cancel-job-fname" type="text" name="fname" value="" />
            <input type="text" name="type" value="job" />
        </form>
    </div>
    <br><br>
    <h2> Kettle Transformation List </h2>
    <table  border="1">
        <tr height="30px">
            <td><h3>Transformation Name</h3></td>
            <td><h3>Parent Job</h3></td>
            <td><h3>Cron Expression</h3></td>
            <td><h3>Next Run Time</h3></td>
            <td><h3>is Running</h3></td>
            <td><h3>Execute</h3></td>
        </tr>
        <c:forEach items="${ trans }" var="tran">
            <tr height="30px">
                <td><c:out value="${tran.name}" /></td>
                <td>
                    <c:forEach items="${ tran.parentJobs }" var="parent">
                        <c:out value="${parent}" /><br>
                    </c:forEach>
                </td>
                <td>Cron Expression</td>
                <td>Next Run Time</td>
                <td><c:out value="${ tran.running }" /></td>
                <td><!-- &nbsp; <input type="button" value="Execute" onclick="exeKettleTran('<c:out value="${tran.name}" />');" /> &nbsp; --></td>
            </tr>
        </c:forEach>
    </table>
    <div style="display: none;">
        <form id="exeKettleTranForm" name="exeKettleTranForm" action="/kettle/executeServlet" method="POST">
            <input id="tran-fname" type="text" name="fname" value="" />
            <input type="text" name="type" value="tran" />
        </form>
    </div>
</body>
</html>